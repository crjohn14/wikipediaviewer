# README #

Wikipedia Viewer
Author: Chris Johnson (https://www.freecodecamp.com/crjohn14)
Project for FreeCodeCamp.com front end development certification

A simple website to interface with the MediaWiki API.  It allows you to search for wikis
or go to a random wiki.